const express = require('express')
const proxy = require('express-http-proxy')

const router = express.Router()


router.all('/user-api/*', proxy(process.env.USER_API))

module.exports = router