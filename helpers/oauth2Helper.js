const Oauth2Tokens = require('../models/Oauth2Tokens')

//Funcao para setar tempo no token.
exports.accessTokenExpiresAt = (tokenType) => {
    console.log('acessando gerador de tempo')
    //15 dias
    const jwtAccessSeconds = 1296000;
    //20 dias
    const jwtRefreshSeconds = 1728000;
    
    let expire = new Date();
    
    if (tokenType == "accessToken") {
        expire.setSeconds(expire.getSeconds() + jwtAccessSeconds);
        return expire.getTime();
    } else {
        expire.setSeconds(expire.getSeconds() + jwtRefreshSeconds);
        return expire.getTime();
    }
}

//Funcao para limpar os tokens no MongoDB
exports.clearTokens = async (user) => {
    console.log('limpando tokens no DB')
    let oldToekns = await Oauth2Tokens.find({"user_id" : user._id})

    if (oldToekns.length > 1) {
        
        return oldToekns.forEach( async token => await Oauth2Tokens.findOneAndDelete({"_id" : token._id}))
    } else {
        
        return Oauth2Tokens.findOneAndDelete({"user_id" : user._id})
    }
}