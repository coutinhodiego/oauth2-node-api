FROM node:alpine

MAINTAINER Diego Coutinho

ENV PORT=4001
ENV MONGO_DB=mongodb://localhost/auth-db
ENV STATIC_TOKEN=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoidG9rZW5TdGF0aWNvIiwiZXhwIjoxLjU1NzM0NDU2NjI4NmUrMjEsImlhdCI6MTU1NjA0ODU2NjAwMDAwMDAwMH0.t8mziiuM982cq7Kl-uALQHGZfby4eokzZX3DsRwWffU
ENV JWT_SECRET=uYv54!w#tr%pl91
ENV JWT_REFRESH_SECRET=#!F4t2Qwd4T57cVbm

COPY . /node/api

EXPOSE $PORT

WORKDIR /node/api

RUN npm install

CMD ["npm", "start"]