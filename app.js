const express = require('express');
const bodyParser = require('body-parser')
const cors = require('cors')
const mongoose = require('mongoose')
const routes = require('./routes/routes')

const app = express();

app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
}))
app.use(bodyParser.json())
app.use(cors());

// ******* OAUTH2 MIDLEWARES *******

require('./oauth2/oauth2')(app)

// ******* OAUTH2 MIDLEWARES *******

//Endpoints...
app.use('/', routes)

//Conectando aplicaçao ao mongoDB
mongoose.connect(process.env.MONGO_DB,  { useNewUrlParser: true } )
    .then((result) => {

        //Abrindo conexao da aplicaçao
        app.listen(process.env.PORT, () => {
            let host = result.connections[0].host

            console.log(`DATA-BASE : ${host}`)
            console.log(`LISTEN ON PORT : ${process.env.PORT}`)
            console.log('OAUTH2 - ONLINE')
        });
    })
    .catch(err => console.log(err))

