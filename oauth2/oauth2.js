const OAuth2Server = require('oauth2-server');
const OAuth2Model = require('./model')
const Request = OAuth2Server.Request;
const Response = OAuth2Server.Response;

module.exports = (app) => {

    //Crian uma nova instancia de OAuthServer
    const oauth = new OAuth2Server({
        model: OAuth2Model
    });
    
    //End point que gera o token para login
    app.post('/oauth2/token', async (req, res, next) => {
        
        let request = new Request(req);    
        let response = new Response(res);

        //Methodo para gerar o token
        try {

            let token = await oauth.token(request, response)

            console.log('token-method')
            
            return res.json({
                accessToken : token.accessToken,
                refreshToken: token.refreshToken
            })

        } catch (err) {

            console.log({status: err.statusCode, msg: err.message})
            return res.json({status: err.statusCode, msg: err.message})

        }
    
        
       
    })
    
    //Midleware que verifica se a requisicao possui token valido.
    app.use( async (req, res, next) => {
        let request = new Request(req);    
        let response = new Response(res);

        try {

            let token = await oauth.authenticate(request, response)
        
            console.log("authenticate-method")
            // console.log(token)
            req.body.user = token.user
            next()

        } catch (err) {

            console.log({status: err.statusCode, msg: err.message})
            return res.json({status: err.statusCode, msg: err.message})

        }
    })
}