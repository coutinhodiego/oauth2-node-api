const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const redis = require('redis')
const redisDb = redis.createClient(process.env.REDIS_URL)

const Oauth2Client = require('../models/Oauth2ClientSchema')
const User = require('../models/UserSchema')
const oauth2Helper = require('../helpers/oauth2Helper')

const JWT_SECRET = process.env.JWT_SECRET
const JWT_REFRESH_SECRET = process.env.JWT_REFRESH_SECRET


//Redis
redisDb.on("error", function (err) {
    console.log("Error " + err);
})


//Verifica Client da requisicao
exports.getClient = async (clientId, clientSecret, callback) => {
    console.log('getClient')

    try {

        let client = await Oauth2Client.findOne({ "client_id" : clientId, "client_secret" : clientSecret })
        callback(null, client)
        
    } catch (err) {

        console.log(err)

    }
}

//Verifica se o usuario 'e valido 
exports.getUser = async (username, password, callback) => {
    console.log('getUser')

    try {

        let user = await User.findOne({"email" : username})
        let passwordVerified = await  bcrypt.compare(password, user.password)
        
        if (passwordVerified) {

            callback(null, user)
        }

        callback(false, null)

    } catch (err) {

        err => console.log(err)

    }
}

// exports.validateScope = (user, client, scope, callback) => {
//     console.log('validateScope')
//     // console.log(user, client, scope)
//     // return callback(null)
// }

//Gera o token de acesso
exports.generateAccessToken = (client, user, callback) => {
    console.log('generateAccessToken')

    let payload = {
        user: user._id
    }

    payload.exp = oauth2Helper.accessTokenExpiresAt('accessToken')

    let options = {
        algorithm: 'HS256'  // HMAC using SHA-256 hash algorithm
    }
    
    let token = jwt.sign(payload, JWT_SECRET, options) 
    return token
}

//Gera o token de atualizacao
exports.generateRefreshToken = (client, user, scope, callback) => {
    console.log('generateRefreshToken')
    
    let payload = {
        user: user._id
    }
    
    payload.exp = oauth2Helper.accessTokenExpiresAt()
    
    let options = {
        algorithm: 'HS256'  // HMAC using SHA-256 hash algorithm
    }
    
    let token = jwt.sign(payload, JWT_REFRESH_SECRET, options)
    callback(null, token)
}

//Salva os token no DB
exports.saveToken = async (token, client, user, callback) => {
    console.log('saveToken')

    try {

        let userId = user._id.toString()
        
        let dataToken = {
            accessToken: token.accessToken,
            refreshToken: token.refreshToken,
            client: client.client_id,
            user: userId,
            accessTokenExpiresAt : new Date(oauth2Helper.accessTokenExpiresAt('accessToken'))
        }

        redisDb.hmset(token.accessToken, dataToken )
        redisDb.expire(token.accessToken, 1296000) // 15 dias de expire
        
        callback(null, dataToken)
        
    } catch (err) {

        err => console.log(err)

    }
}

exports.getAccessToken = (accessToken, callback) => {
    console.log('getAccessToken')

    try {

        // Verificacao do token no redis
        redisDb.hgetall(accessToken, (err, token) => {
                
            if (!token) {
                return callback(err, null)
            }
            
            callback(null, {
                accessToken : token.accessToken,
                user : token.user,
                accessTokenExpiresAt : new Date(token.accessTokenExpiresAt)
            })
        })

    } catch (err) {

        console.log(err)

    }

}

// exports.verifyScope = async (accessToken, scope, callback) => {
//     console.log('verifyScope')
// }